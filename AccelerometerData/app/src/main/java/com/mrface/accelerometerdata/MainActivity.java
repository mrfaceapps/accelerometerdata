package com.mrface.accelerometerdata;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.io.File;


public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private static String[] PERMISSIONS_STORAGE = new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private String CUSTOM_DATA_SEPARATOR = " ";
    private boolean CUSTOM_REMOVE_G = false;
    private int CUSTOM_SENSOR_SPEED = 3;
    private int CUSTOM_SENSOR_UNIT = 0;
    private String TAG = "AccelerometerData";
    private int dataSeparatorSetting = 0;
    private Spinner dataSeparatorSpinner;
    private Spinner exportFileSpinner;
    private int exportFileFormat = 0;
    private Intent dataWritingService;
    private File directory;
    private String fileName = "data";
    private EditText fileNameEdit;
    private String fileNameS = "data";
    private int fileVersion = 0;
    private float[] gravity = new float[]{0.0f, 0.0f, 0.0f};
    long lastTimeUpdate = 0;
    private boolean recording = false;
    private CheckBox removeGCheckBox;
    private Sensor senAccelerometer;
    private SensorManager senSensorManager;
    private LineChart sensorLineChart = null;
    private Spinner sensorSpeedSpinner;
    private Spinner sensorUnitSpinner;
    CheckBox xAxis;
    private float xAxisNumber;
    private int xSeriesColor = 0;
    CheckBox yAxis;
    private float yAxisNumber;
    private VerticalLabelView yAxisTitle;
    private int ySeriesColor = 0;
    CheckBox zAxis;
    private float zAxisNumber;
    private int zSeriesColor = 0;

    class C02341 implements OnCheckedChangeListener {
        C02341() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(sensorLineChart != null && sensorLineChart.getData() != null && sensorLineChart.getData().getDataSetByIndex(0) != null){
                sensorLineChart.getData().getDataSetByIndex(0).setVisible(isChecked);
            }
        }
    }

    class C02352 implements OnCheckedChangeListener {
        C02352() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(sensorLineChart != null && sensorLineChart.getData() != null && sensorLineChart.getData().getDataSetByIndex(1) != null){
                sensorLineChart.getData().getDataSetByIndex(1).setVisible(isChecked);
            }
        }
    }

    class C02363 implements OnCheckedChangeListener {
        C02363() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(sensorLineChart != null && sensorLineChart.getData() != null && sensorLineChart.getData().getDataSetByIndex(2) != null){
                sensorLineChart.getData().getDataSetByIndex(2).setVisible(isChecked);
            }
        }
    }

    class C02394 implements OnMenuItemClickListener {

        class C02371 implements OnClickListener {
            C02371() {
            }

            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.fileName = MainActivity.this.fileNameEdit.getText().toString();
                MainActivity.this.fileNameS = MainActivity.this.fileNameEdit.getText().toString();
                String obj = MainActivity.this.dataSeparatorSpinner.getSelectedItem().toString();

                switch (obj.toLowerCase()) {
                    case "space":
                        MainActivity.this.CUSTOM_DATA_SEPARATOR = " ";
                        break;
                    case "comma":
                        MainActivity.this.CUSTOM_DATA_SEPARATOR = ",";
                        break;
                    case "semicolon":
                        MainActivity.this.CUSTOM_DATA_SEPARATOR = ";";
                        break;
                    default:
                        MainActivity.this.CUSTOM_DATA_SEPARATOR = " ";
                        break;
                }
                MainActivity.this.dataSeparatorSetting = MainActivity.this.dataSeparatorSpinner.getSelectedItemPosition();
                MainActivity.this.setWritePreferences();
                dialog.dismiss();
                Toast.makeText(MainActivity.this, "File name is " + MainActivity.this.fileNameEdit.getText().toString() + ". Data separator is a " + MainActivity.this.dataSeparatorSpinner.getSelectedItem().toString() + ".", Toast.LENGTH_LONG).show();
            }
        }

        class C02382 implements OnClickListener {
            C02382() {
            }

            public void onClick(DialogInterface dialog, int which) {
                if (MainActivity.this.recording) {
                    Toast.makeText(MainActivity.this, "Saving in progress. Unable to change sensor information.", Toast.LENGTH_LONG).show();
                } else {
                    MainActivity.this.CUSTOM_SENSOR_SPEED = MainActivity.this.sensorSpeedSpinner.getSelectedItemPosition();
                    MainActivity.this.senSensorManager.unregisterListener(MainActivity.this);
                    MainActivity.this.senSensorManager.registerListener(MainActivity.this, MainActivity.this.senAccelerometer, MainActivity.this.CUSTOM_SENSOR_SPEED);
                    MainActivity.this.CUSTOM_SENSOR_UNIT = MainActivity.this.sensorUnitSpinner.getSelectedItemPosition();
                    MainActivity.this.CUSTOM_REMOVE_G = MainActivity.this.removeGCheckBox.isChecked();
                    MainActivity.this.unitChange(MainActivity.this.CUSTOM_SENSOR_UNIT);
                    xAxis.setChecked(true);
                    yAxis.setChecked(true);
                    zAxis.setChecked(true);
                    Toast.makeText(MainActivity.this, "Sensor speed is " + MainActivity.this.sensorSpeedSpinner.getSelectedItem().toString() + "\nSensor Unit is: " + MainActivity.this.sensorUnitSpinner.getSelectedItem().toString() + "\nRemove G is set to: " + String.valueOf(MainActivity.this.removeGCheckBox.isChecked()), Toast.LENGTH_LONG).show();
                }
                MainActivity.this.setSensorPreferences();
                dialog.dismiss();
            }
        }

        C02394() {
        }

        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case 0:
                    Builder alertDialogBuilderWS = new Builder(MainActivity.this.getSupportActionBar().getThemedContext());
                    View wsDialogView = MainActivity.this.getLayoutInflater().inflate(R.layout.ws_dialog, null);
                    MainActivity.this.fileNameEdit = (EditText) wsDialogView.findViewById(R.id.fileNameEdit);
                    MainActivity.this.fileNameEdit.append(MainActivity.this.fileNameS);
                    MainActivity.this.dataSeparatorSpinner = (Spinner) wsDialogView.findViewById(R.id.separatorSpinner);
                    MainActivity.this.exportFileSpinner = (Spinner) wsDialogView.findViewById(R.id.formatSpinner);
                    MainActivity.this.exportFileSpinner.setSelection(MainActivity.this.exportFileFormat);
                    MainActivity.this.dataSeparatorSpinner.setSelection(MainActivity.this.dataSeparatorSetting);
                    MainActivity.this.exportFileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            exportFileFormat = i;
                            if(i == 1){
                                MainActivity.this.dataSeparatorSetting = 1;
                                MainActivity.this.dataSeparatorSpinner.setSelection(MainActivity.this.dataSeparatorSetting);
                                MainActivity.this.dataSeparatorSpinner.setEnabled(false);
                            }else{
                                MainActivity.this.dataSeparatorSpinner.setEnabled(true);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    alertDialogBuilderWS.setTitle((CharSequence) "Write data Settings");
                    alertDialogBuilderWS.setView(wsDialogView);
                    alertDialogBuilderWS.setPositiveButton((CharSequence) "Save & Close", new C02371());
                    alertDialogBuilderWS.create().show();
                    break;
                case 1:
                    Builder alertDialogBuilderSS = new Builder(MainActivity.this.getSupportActionBar().getThemedContext());
                    View ssDialogView = MainActivity.this.getLayoutInflater().inflate(R.layout.s_dialog, null);
                    MainActivity.this.sensorSpeedSpinner = (Spinner) ssDialogView.findViewById(R.id.sensorSpeedSpinner);
                    MainActivity.this.sensorSpeedSpinner.setSelection(MainActivity.this.CUSTOM_SENSOR_SPEED);
                    MainActivity.this.sensorUnitSpinner = (Spinner) ssDialogView.findViewById(R.id.sensorUnitSpinner);
                    MainActivity.this.sensorUnitSpinner.setSelection(MainActivity.this.CUSTOM_SENSOR_UNIT);
                    MainActivity.this.removeGCheckBox = (CheckBox) ssDialogView.findViewById(R.id.sensorRemoveGCheckbox);
                    MainActivity.this.removeGCheckBox.setChecked(MainActivity.this.CUSTOM_REMOVE_G);
                    alertDialogBuilderSS.setTitle(MainActivity.this.getString(R.string.sensorSettingsTitle));
                    alertDialogBuilderSS.setView(ssDialogView);
                    alertDialogBuilderSS.setPositiveButton((CharSequence) "Save & Close", new C02382());
                    alertDialogBuilderSS.create().show();
                    break;
                default:
                    Toast.makeText(MainActivity.this, "Default", Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, Toast.LENGTH_LONG);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == 0) {
                    this.directory = new File("/sdcard/" + "accelerometerDataDir");
                    if (!this.directory.exists()) {
                        this.directory.mkdirs();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, "207097864", false);
        StartAppSDK.setUserConsent (this, "pas",System.currentTimeMillis(),false);
        StartAppAd.disableSplash();
        verifyStoragePermissions(this);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        readWritePreferences();
        readSensorPreferences();
        this.xAxis = (CheckBox) findViewById(R.id.xAxis);
        this.yAxis = (CheckBox) findViewById(R.id.yAxis);
        this.zAxis = (CheckBox) findViewById(R.id.zAxis);
        this.xAxis.setChecked(true);
        this.yAxis.setChecked(true);
        this.zAxis.setChecked(true);
        this.xSeriesColor = Color.rgb(0, 153, 0);
        this.ySeriesColor = Color.rgb(0, 81, 255);
        this.zSeriesColor = Color.rgb(230, 0, 0);
        this.dataWritingService = new Intent(this, SensorService.class);
        this.senSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.senAccelerometer = this.senSensorManager.getDefaultSensor(1);
        this.lastTimeUpdate = System.currentTimeMillis();
        this.sensorLineChart = (LineChart) findViewById(R.id.accelerometerLineChart);
        this.sensorLineChart.setDrawGridBackground(false);
        this.sensorLineChart.setDescription(new Description());
        this.sensorLineChart.setNoDataText("You need to provide data for the chart.");
        this.sensorLineChart.setTouchEnabled(true);
        this.sensorLineChart.setDragEnabled(true);
        this.sensorLineChart.setScaleEnabled(false);
        this.sensorLineChart.setDrawGridBackground(false);
        this.sensorLineChart.getLegend().setEnabled(false);
        LineData data = new LineData();
        data.setValueTextColor(-1);
        this.sensorLineChart.setData(data);
        this.yAxisTitle = (VerticalLabelView) findViewById(R.id.yAxisTitle);
        this.yAxisTitle.setText("m/Sec^2");
        this.sensorLineChart.getXAxis().setEnabled(false);
        YAxis leftAxis = this.sensorLineChart.getAxisLeft();
        leftAxis.setTextColor(-1);
        leftAxis.setDrawGridLines(true);
        this.sensorLineChart.getAxisRight().setEnabled(false);
        unitChange(this.CUSTOM_SENSOR_UNIT);
        this.xAxis.setOnCheckedChangeListener(new C02341());
        this.yAxis.setOnCheckedChangeListener(new C02352());
        this.zAxis.setOnCheckedChangeListener(new C02363());
    }

    private void addEntries() {
        LineData data = (LineData) this.sensorLineChart.getData();
        if (data != null) {
            ILineDataSet xDataSet = (ILineDataSet) data.getDataSetByIndex(0);
            ILineDataSet yDataSet = (ILineDataSet) data.getDataSetByIndex(1);
            ILineDataSet zDataSet = (ILineDataSet) data.getDataSetByIndex(2);
            if (xDataSet == null) {
                xDataSet = createSet(this.xSeriesColor);
                data.addDataSet(xDataSet);
            }
            if (yDataSet == null) {
                yDataSet = createSet(this.ySeriesColor);
                data.addDataSet(yDataSet);
            }
            if (zDataSet == null) {
                zDataSet = createSet(this.zSeriesColor);
                data.addDataSet(zDataSet);
            }
            data.addEntry(new Entry((float) xDataSet.getEntryCount(), this.xAxisNumber), 0);
            data.addEntry(new Entry((float) yDataSet.getEntryCount(), this.yAxisNumber), 1);
            data.addEntry(new Entry((float) zDataSet.getEntryCount(), this.zAxisNumber), 2);
            data.notifyDataChanged();
            this.sensorLineChart.notifyDataSetChanged();
            if (this.xAxisNumber > this.sensorLineChart.getAxisLeft().getAxisMaximum() || this.yAxisNumber > this.sensorLineChart.getAxisLeft().getAxisMaximum() || this.zAxisNumber > this.sensorLineChart.getAxisLeft().getAxisMaximum()) {
                this.sensorLineChart.getAxisLeft().setAxisMaxValue(Math.max(Math.max(this.xAxisNumber, this.yAxisNumber), this.zAxisNumber) + 1.0f);
                this.sensorLineChart.getAxisLeft().setAxisMinValue(-(Math.max(Math.max(this.xAxisNumber, this.yAxisNumber), this.zAxisNumber) + 1.0f));
            }
            this.sensorLineChart.setVisibleXRangeMinimum(50.0f);
            this.sensorLineChart.setVisibleXRangeMaximum(50.0f);
            this.sensorLineChart.moveViewToX((float) data.getEntryCount());
        }
    }

    private static LineDataSet createSet(int color) {
        LineDataSet set = new LineDataSet(null, "");
        set.setAxisDependency(AxisDependency.LEFT);
        set.setColor(color);
        set.setDrawCircles(false);
        set.setHighlightEnabled(false);
        set.setLineWidth(2.0f);
        set.setValueTextColor(-1);
        set.setDrawValues(false);
        return set;
    }

    protected void onResume() {
        this.senSensorManager.registerListener(this, this.senAccelerometer, this.CUSTOM_SENSOR_SPEED);
        this.directory = new File("/sdcard/" + "accelerometerDataDir");
        if (!this.directory.exists()) {
            this.directory.mkdirs();
        }
        super.onResume();
    }

    protected void onPause() {
        this.senSensorManager.unregisterListener(this);
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.savingDataBtn:
                if (this.recording) {
                    item.setIcon(ContextCompat.getDrawable(MainActivity.this,android.R.drawable.ic_menu_save));
                    stopService(this.dataWritingService);
                    this.recording = false;
                    Toast.makeText(this, "Finished saving data!", Toast.LENGTH_SHORT).show();

                    if(getSharedPreferences("main",MODE_PRIVATE).getInt("exportCount",-1) == 5){
                        //Show ad
                        StartAppAd.showAd(MainActivity.this);

                        getSharedPreferences("main",MODE_PRIVATE).edit().putInt("exportCount",0).apply();
                    }else{
                        getSharedPreferences("main",MODE_PRIVATE).edit().putInt("exportCount",getSharedPreferences("main",MODE_PRIVATE).getInt("exportCount",0) + 1).apply();
                    }
                    return true;
                }
                this.recording = true;
                String fileExtension = "." + getResources().getStringArray(R.array.fileFormatSpinnerValues)[exportFileFormat];
                File file = new File(this.directory.getAbsolutePath(), this.fileName + fileExtension);
                if (file.exists()) {
                    file = new File(this.directory.getAbsolutePath(), this.fileName + fileExtension);
                    if (file.exists()) {
                        while (file.exists()) {
                            StringBuilder append = new StringBuilder().append(this.fileNameS).append("_");
                            int i = this.fileVersion;
                            this.fileVersion = i + 1;
                            this.fileName = append.append(Integer.toString(i)).toString();
                            file = new File(this.directory.getAbsolutePath(), this.fileName + fileExtension);
                        }
                        item.setIcon(ContextCompat.getDrawable(MainActivity.this,android.R.drawable.ic_media_pause));
                        this.dataWritingService.putExtra(getString(R.string.TAG_FILE), file);
                        this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_DATA_SEPARATOR), this.CUSTOM_DATA_SEPARATOR);
                        this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_SENSOR_SPEED), this.CUSTOM_SENSOR_SPEED);
                        this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_SENSOR_UNIT), this.CUSTOM_SENSOR_UNIT);
                        this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_REMOVE_G), this.CUSTOM_REMOVE_G);
                        startService(this.dataWritingService);
                    }
                } else {
                    this.dataWritingService.putExtra(getString(R.string.TAG_FILE), file);
                    this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_DATA_SEPARATOR), this.CUSTOM_DATA_SEPARATOR);
                    this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_SENSOR_SPEED), this.CUSTOM_SENSOR_SPEED);
                    this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_SENSOR_UNIT), this.CUSTOM_SENSOR_UNIT);
                    this.dataWritingService.putExtra(getString(R.string.TAG_CUSTOM_REMOVE_G), this.CUSTOM_REMOVE_G);
                    startService(this.dataWritingService);
                    item.setIcon(ContextCompat.getDrawable(MainActivity.this,android.R.drawable.ic_media_pause));
                }
                Toast.makeText(this, "Saving data to: " + this.fileName, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.preferencesBtn:
                PopupMenu settingsPopupmenu = new PopupMenu(this, findViewById(R.id.preferencesBtn));
                settingsPopupmenu.getMenu().add(1, 0, 1, "Write Settings");
                settingsPopupmenu.getMenu().add(1, 1, 2, "Sensor Settings");
                settingsPopupmenu.setOnMenuItemClickListener(new C02394());
                settingsPopupmenu.show();
                return true;
            default:
                Log.i(this.TAG, "Error, No valid button press");
                return super.onOptionsItemSelected(item);
        }
    }

    public synchronized void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            long curTime = System.currentTimeMillis();
            if (curTime - this.lastTimeUpdate > ((long) 100)) {
                this.lastTimeUpdate = curTime;
                if (this.CUSTOM_REMOVE_G) {
                    this.gravity[0] = (this.gravity[0] * 0.9f) + ((1.0f - 0.9f) * event.values[0]);
                    this.gravity[1] = (this.gravity[1] * 0.9f) + ((1.0f - 0.9f) * event.values[1]);
                    this.gravity[2] = (this.gravity[2] * 0.9f) + ((1.0f - 0.9f) * event.values[2]);
                    this.xAxisNumber = event.values[0] - this.gravity[0];
                    this.yAxisNumber = event.values[1] - this.gravity[1];
                    this.zAxisNumber = event.values[2] - this.gravity[2];
                } else {
                    this.xAxisNumber = event.values[0];
                    this.yAxisNumber = event.values[1];
                    this.zAxisNumber = event.values[2];
                }
                switch (this.CUSTOM_SENSOR_UNIT) {
                    case 0:
                        this.xAxisNumber = UnitRecalculate.metersToMeters(this.xAxisNumber);
                        this.yAxisNumber = UnitRecalculate.metersToMeters(this.yAxisNumber);
                        this.zAxisNumber = UnitRecalculate.metersToMeters(this.zAxisNumber);
                        break;
                    case 1:
                        this.xAxisNumber = UnitRecalculate.metersToFeet(this.xAxisNumber);
                        this.yAxisNumber = UnitRecalculate.metersToFeet(this.yAxisNumber);
                        this.zAxisNumber = UnitRecalculate.metersToFeet(this.zAxisNumber);
                        break;
                    case 2:
                        this.xAxisNumber = UnitRecalculate.metersToGravity(this.xAxisNumber);
                        this.yAxisNumber = UnitRecalculate.metersToGravity(this.yAxisNumber);
                        this.zAxisNumber = UnitRecalculate.metersToGravity(this.zAxisNumber);
                        break;
                    default:
                        this.xAxisNumber = UnitRecalculate.metersToMeters(this.xAxisNumber);
                        this.yAxisNumber = UnitRecalculate.metersToMeters(this.yAxisNumber);
                        this.zAxisNumber = UnitRecalculate.metersToMeters(this.zAxisNumber);
                        break;
                }
                this.xAxis.setText(new StringBuilder().append("x: ").append(String.valueOf(this.xAxisNumber)));
                this.yAxis.setText(new StringBuilder().append("y: ").append(String.valueOf(this.yAxisNumber)));
                this.zAxis.setText(new StringBuilder().append("z: ").append(String.valueOf(this.zAxisNumber)));
                addEntries();
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void resetSeriesOnChart() {
        this.sensorLineChart.getLineData().clearValues();
    }

    public void unitChange(int unit) {
        switch (unit) {
            case 0:
                this.yAxisTitle.setText(getString(R.string.metersPerSec));
                this.sensorLineChart.getAxisLeft().setAxisMaxValue(10.0f);
                this.sensorLineChart.getAxisLeft().setAxisMinValue(-10.0f);
                break;
            case 1:
                this.yAxisTitle.setText(getString(R.string.feetPerSec));
                this.sensorLineChart.getAxisLeft().setAxisMaxValue(35.0f);
                this.sensorLineChart.getAxisLeft().setAxisMinValue(-35.0f);
                break;
            case 2:
                this.yAxisTitle.setText(getString(R.string.gravity));
                this.sensorLineChart.getAxisLeft().setAxisMaxValue(1.0f);
                this.sensorLineChart.getAxisLeft().setAxisMinValue(-1.0f);
                break;
            default:
                this.yAxisTitle.setText(getString(R.string.metersPerSec));
                this.sensorLineChart.getAxisLeft().setAxisMaxValue(10.0f);
                this.sensorLineChart.getAxisLeft().setAxisMinValue(-10.0f);
                break;
        }
        resetSeriesOnChart();
    }

    public void setWritePreferences() {
        Editor editor = getSharedPreferences(getString(R.string.TAG_writeSettingsPreference), 0).edit();
        editor.putInt(getString(R.string.TAG_fileFormatPreference),this.exportFileFormat);
        editor.putString(getString(R.string.TAG_fileNamePreference), this.fileNameS);
        editor.putInt(getString(R.string.TAG_dataSeparatorPreference), this.dataSeparatorSetting);
        editor.apply();
    }

    public void readWritePreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.TAG_writeSettingsPreference), 0);
        if (sharedPreferences.contains(getString(R.string.TAG_fileNamePreference))) {
            this.fileName = sharedPreferences.getString(getString(R.string.TAG_fileNamePreference), "NULL");
            this.fileNameS = sharedPreferences.getString(getString(R.string.TAG_fileNamePreference), "NULL");
            this.fileNameS = sharedPreferences.getString(getString(R.string.TAG_fileNamePreference), "NULL");
        }
        if (sharedPreferences.contains(getString(R.string.TAG_dataSeparatorPreference))) {
            this.dataSeparatorSetting = sharedPreferences.getInt(getString(R.string.TAG_dataSeparatorPreference), 0);
        }
        if(sharedPreferences.contains(getString(R.string.TAG_fileFormatPreference))){
            this.exportFileFormat = sharedPreferences.getInt(getString(R.string.TAG_fileFormatPreference),0);
        }
    }

    public void setSensorPreferences() {
        Editor editor = getSharedPreferences(getString(R.string.TAG_sensorSettingsPreference), 0).edit();
        editor.putInt(getString(R.string.TAG_sensorSpeedPreference), this.CUSTOM_SENSOR_SPEED);
        editor.putInt(getString(R.string.TAG_sensorUnitPreference), this.CUSTOM_SENSOR_UNIT);
        editor.putBoolean(getString(R.string.TAG_removeGPreference), this.CUSTOM_REMOVE_G);
        editor.apply();
    }

    public void readSensorPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.TAG_sensorSettingsPreference), 0);
        if (sharedPreferences.contains(getString(R.string.TAG_sensorSpeedPreference))) {
            this.CUSTOM_SENSOR_SPEED = sharedPreferences.getInt(getString(R.string.TAG_sensorSpeedPreference), 0);
        }
        if (sharedPreferences.contains(getString(R.string.TAG_sensorUnitPreference))) {
            this.CUSTOM_SENSOR_UNIT = sharedPreferences.getInt(getString(R.string.TAG_sensorUnitPreference), 0);
        }
        if (sharedPreferences.contains(getString(R.string.TAG_removeGPreference))) {
            this.CUSTOM_REMOVE_G = sharedPreferences.getBoolean(getString(R.string.TAG_removeGPreference), false);
        }
    }
}
