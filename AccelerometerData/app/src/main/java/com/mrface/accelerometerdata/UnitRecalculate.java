package com.mrface.accelerometerdata;

public class UnitRecalculate {
    public void UnitRecalculate() {
    }

    public static float metersToMeters(float number) {
        return round(number, 3);
    }

    public static float metersToFeet(float number) {
        return round(3.28084f * number, 3);
    }


    public static float metersToGravity(float number) {
        return round(0.1019368f * number, 3);
    }

    public static float round(float value, int dp) {
        if (dp < 0) {
            throw new IllegalArgumentException();
        }
        long factor = (long) Math.pow(10.0d, (double) dp);
        return ((float) ((long) Math.round(value * ((float) factor)))) / ((float) factor);
    }
}
