package com.mrface.accelerometerdata;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

public class SensorService extends Service implements SensorEventListener {
    private String CUSTOM_DATA_SEPARATOR = " ";
    private Boolean CUSTOM_SENSOR_REMOVE_G = Boolean.valueOf(false);
    private int CUSTOM_SENSOR_SPEED = 3;
    private String CUSTOM_SENSOR_SPEED_DEF = "SENSOR_DELAY_NORMAL";
    private int CUSTOM_SENSOR_UNIT = 0;
    private String CUSTOM_SENSOR_UNIT_DEF = "Meters/Sec^2";
    private FileOutputStream fileOutputStream;
    private boolean fileOutputStreamClosed = true;
    private float[] gravity = new float[]{0.0f, 0.0f, 0.0f};
    private long lastUpdate;
    private Sensor senAccelerometer;
    private SensorManager senSensorManager;

    public void onCreate() {
        super.onCreate();
        this.senSensorManager = (SensorManager) getSystemService("sensor");
        this.senAccelerometer = this.senSensorManager.getDefaultSensor(1);
        this.lastUpdate = System.currentTimeMillis();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra("FILE") && intent.hasExtra("CUSTOM_DATA_SEPARATOR") && intent.hasExtra("CUSTOM_SENSOR_SPEED") && intent.hasExtra("CUSTOM_SENSOR_UNIT") && intent.hasExtra("CUSTOM_REMOVE_G")) {
            File file = (File) intent.getExtras().get("FILE");
            this.CUSTOM_DATA_SEPARATOR = intent.getStringExtra("CUSTOM_DATA_SEPARATOR");
            switch (intent.getIntExtra("CUSTOM_SENSOR_SPEED", 3)) {
                case 0:
                    this.CUSTOM_SENSOR_SPEED = 0;
                    this.CUSTOM_SENSOR_SPEED_DEF = "SENSOR_DELAY_FASTEST";
                    break;
                case 1:
                    this.CUSTOM_SENSOR_SPEED = 1;
                    this.CUSTOM_SENSOR_SPEED_DEF = "SENSOR_DELAY_GAME";
                    break;
                case 2:
                    this.CUSTOM_SENSOR_SPEED = 2;
                    this.CUSTOM_SENSOR_SPEED_DEF = "SENSOR_DELAY_UI";
                    break;
                case 3:
                    this.CUSTOM_SENSOR_SPEED = 3;
                    this.CUSTOM_SENSOR_SPEED_DEF = "SENSOR_DELAY_NORMAL";
                    break;
            }
            this.CUSTOM_SENSOR_UNIT = intent.getIntExtra("CUSTOM_SENSOR_UNIT", 0);
            switch (this.CUSTOM_SENSOR_UNIT) {
                case 0:
                    this.CUSTOM_SENSOR_UNIT_DEF = "Meters/Sec^2";
                    break;
                case 1:
                    this.CUSTOM_SENSOR_UNIT_DEF = "Feet/Sec^2";
                    break;
                case 2:
                    this.CUSTOM_SENSOR_UNIT_DEF = "Gravity";
                    break;
                default:
                    this.CUSTOM_SENSOR_UNIT_DEF = "Meters/Sec^2";
                    break;
            }
            this.CUSTOM_SENSOR_REMOVE_G = Boolean.valueOf(intent.getBooleanExtra("CUSTOM_REMOVE_G", false));
            try {
                this.fileOutputStream = new FileOutputStream(file);
                this.fileOutputStream.write(("## Started recording at: " + DateFormat.getDateTimeInstance().format(new Date()) + "\n").getBytes());
                this.fileOutputStream.write(("## Sensor Speed is " + this.CUSTOM_SENSOR_SPEED_DEF + "\n").getBytes());
                this.fileOutputStream.write(("## Sensor Unit is " + this.CUSTOM_SENSOR_UNIT_DEF + "\n").getBytes());
                this.fileOutputStream.write("## xAxis Value, yAxis Value, zAxis Value \n".getBytes());
                this.fileOutputStreamClosed = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.senSensorManager.registerListener(this, this.senAccelerometer, this.CUSTOM_SENSOR_SPEED);
        }
        return 2;
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1 && System.currentTimeMillis() - this.lastUpdate > ((long) 60)) {
            Float xAxisNumber;
            Float yAxisNumber;
            Float zAxisNumber;
            this.lastUpdate = System.currentTimeMillis();
            if (this.CUSTOM_SENSOR_REMOVE_G.booleanValue()) {
                this.gravity[0] = (this.gravity[0] * 0.9f) + ((1.0f - 0.9f) * event.values[0]);
                this.gravity[1] = (this.gravity[1] * 0.9f) + ((1.0f - 0.9f) * event.values[1]);
                this.gravity[2] = (this.gravity[2] * 0.9f) + ((1.0f - 0.9f) * event.values[2]);
                xAxisNumber = Float.valueOf(event.values[0] - this.gravity[0]);
                yAxisNumber = Float.valueOf(event.values[1] - this.gravity[1]);
                zAxisNumber = Float.valueOf(event.values[2] - this.gravity[2]);
            } else {
                xAxisNumber = Float.valueOf(event.values[0]);
                yAxisNumber = Float.valueOf(event.values[1]);
                zAxisNumber = Float.valueOf(event.values[2]);
            }
            switch (this.CUSTOM_SENSOR_UNIT) {
                case 0:
                    xAxisNumber = Float.valueOf(UnitRecalculate.metersToMeters(xAxisNumber.floatValue()));
                    yAxisNumber = Float.valueOf(UnitRecalculate.metersToMeters(yAxisNumber.floatValue()));
                    zAxisNumber = Float.valueOf(UnitRecalculate.metersToMeters(zAxisNumber.floatValue()));
                    break;
                case 1:
                    xAxisNumber = Float.valueOf(UnitRecalculate.metersToFeet(xAxisNumber.floatValue()));
                    yAxisNumber = Float.valueOf(UnitRecalculate.metersToFeet(yAxisNumber.floatValue()));
                    zAxisNumber = Float.valueOf(UnitRecalculate.metersToFeet(zAxisNumber.floatValue()));
                    break;
                case 2:
                    xAxisNumber = Float.valueOf(UnitRecalculate.metersToGravity(xAxisNumber.floatValue()));
                    yAxisNumber = Float.valueOf(UnitRecalculate.metersToGravity(yAxisNumber.floatValue()));
                    zAxisNumber = Float.valueOf(UnitRecalculate.metersToGravity(zAxisNumber.floatValue()));
                    break;
                default:
                    xAxisNumber = Float.valueOf(UnitRecalculate.metersToMeters(xAxisNumber.floatValue()));
                    yAxisNumber = Float.valueOf(UnitRecalculate.metersToMeters(yAxisNumber.floatValue()));
                    zAxisNumber = Float.valueOf(UnitRecalculate.metersToMeters(zAxisNumber.floatValue()));
                    break;
            }
            try {
                if(this.fileOutputStream != null){
                    this.fileOutputStream.write((String.valueOf(xAxisNumber) + this.CUSTOM_DATA_SEPARATOR + String.valueOf(yAxisNumber) + this.CUSTOM_DATA_SEPARATOR + String.valueOf(zAxisNumber) + "\n").getBytes());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onDestroy() {
        if (!this.fileOutputStreamClosed) {
            try {
                this.fileOutputStream.write(("## Stopped recording at: " + DateFormat.getDateTimeInstance().format(new Date()) + "\n").getBytes());
                this.fileOutputStream.close();
                this.fileOutputStreamClosed = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.senSensorManager.unregisterListener(this);
        super.onDestroy();
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }
}
